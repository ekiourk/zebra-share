# Zebra Portal

This repository contains code related to the Zebra Portal

## Development

This project assumes you have [NodeJS](https://nodejs.org) v 10.14.2 installed.
Additionally, ensure you have
installed the [Yarn](https://yarnpkg.com) package manager.  We don't support NPM, Yarn keeps environments deterministic and installations speedy,
so use it.

To begin, run the following the first time you sync the repository, or when
new dependencies are introduced:

    $ yarn install

Once dependencies are installed, run the following to start the development
instance:

    $ yarn start

This will start the development server and will open it in your local web
browser.  Changes will be reflected instantly in the browser.

To familiarize yourself generally with the tech stack and choices here,
read teh [intro docs](docs/INTRO.md).

## Testing

It's important to ensure we maintain quality test coverage.  When checking
new code in, ensure it is covered by tests.  When developing, it can be useful
to run the test runner in another terminal, as it will observe changed files
and rerun the relevant tests, providing instance feedback.

    $ yarn run test

## Deployment

Deployment is handled automatically for you, but more complete information
on deployment can be found [here](docs/DEPLOYMENT.md).

## More Information

One can read more high-level information about developing the application at
the auto-generated [create-react-app docs](docs/CREATEREACTAPP.md).  It
contains useful information on testing, adding dependencies, and general
development help.  It's worth a read if you've never worked with a
`create-react-app` generated project before.
