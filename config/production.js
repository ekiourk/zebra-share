window.ZEBRA_CONFIG = {
    ENV: 'production',
    SERVER_URL: 'https://api.zebrapp.io',
    WS_SERVER_URL: 'wss://api.zebrapp.io',
    MIXPANEL_TOKEN: '7b370d1aba51c02590131c1fc82ea13d',
    SENTRY_URL: 'https://10cb09577aae4b049a52287ed8b9ab13@sentry.io/1241525',
};
