# Zebra Portal Deployment

This document outlines the deployment of the share app.

## Infrastructure

The following assumes you are in the `terraform` directory in this
repository.

### Configuring Terraform

Our Terraform setup will look for AWS credentials in the `zebra`
[profile](https://docs.aws.amazon.com/cli/latest/userguide/cli-multiple-profiles.html).
Unfortunately due to how Terraform loads its files, we cannot
dynamically set this value as the s3 backend depends on it.

### Initializing state

Terraform relies on a state file to determine what the infrastructure
graph looks like. In order to initialize this, run the following:

    $ terraform init

Note that it's wise to check to make sure nobody else is modifying
the state at the same time, only one person should make changes.

### Configuring the Infrastructure

To deploy changes to the infrastructure, first see what would happen:

    $  terraform plan

This would tell you what changes would be made.  If you are satisfied,
then `apply` them:

    $ terraform apply

At the end of this, if all goes well, there should be output that is
useful for configuring other services.  Currently we are only using
this to configure the share app CI

## Continuous Deployment

As part of our CI process, upon successful build and test we should
deploy the built assets to our s3 bucket. CI/CD runs on Travis, and
details of what we do can be found in the [.travis.yml](../.travis.yml)
file.

### Configuring CD

There are a few necessary configuration options which must be set for
the deployment to work. The values for these configuration options can
be found by running `terraform output`.  Now, back in the root
of the repo, peered witih `.travis.yml`, you'll need to ensure your
environment variables are set correctly.  There should be one in
the `.travis.yml` called `AWS_CLOUDFRONT_DISTRIBUTION_ID`, ensure
that matches what was output from `terraform output`.  There are other
environment variables, ensure they match the expected values as well.

Next, we'll encrypt the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`.
Ensure that you've deleted existing secure environment variables first, then
run the following, replacing the `XXXXX` placeholders with the relevant
output from `terraform output`:

   $ travis encrypt AWS_ACCESS_KEY_ID="XXXXX" --add
   $ travis encrypt 'AWS_SECRET_ACCESS_KEY=XXXXX' --add

Some notes here - first note that the second one, for `AWS_SECRET_ACCESS_KEY`
is in single quotes.  This is to simplify escaping.  Second, related to
said escaping, ensure you escape special characters so that they are
properly captured by the command.
