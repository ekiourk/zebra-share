# An Introduction to React, Redux, and Associated Tech

This document seeks to provide a brief overview of some of the technology
in our stack, as well as familiarize you with the layout and implementation
we've put in place.

## High Level Landscape

At a high level, the most useful distinction here is between React and Redux.
React is essentially a library only concerned with the presentation layer -
it does have some data storage and manipulation features, specifically each
component can optionally have internal state, but that is primarily the
domain of Redux, which controls global application state, and allows you to
pass parts of it into your components.

If there's anything to take away from this it's that - React code should
be kept as simple and dumb as possible - if it can be a simple function
it should be.  State and data maniuplation should be handled by Redux,
with Redux actions (which are simply JavaScript Objects describing
*what* should change) and Reducers (which take actions and decide *how*
our state should change - these are always pure functions, no external
calls) doing all the work of making the application interesting.

Within React, we use several libraries worth being familiar with:

- [React-Semantic-UI](https://react.semantic-ui.com/):
Simple, declarative UI components.
- [Radium](http://formidable.com/open-source/radium/):
For component styling. Also used in the app.
- [React Router](https://reacttraining.com/react-router/):
Declarative Routing - think individual pages. Also used in the app, although
this version is newer.

Within Redux we use a few libraries of note as well:

- [Redux Thunk](https://github.com/gaearon/redux-thunk):
Middleware that allows us to return functions instead of actions
- [Redux Actions](https://redux-actions.js.org/):
Provide a standardized schema for our actions

## A Tour of the `src` Directory

As compared to the initial check-in, it might seem a bit overwhelming
how many directories have appeared.  React/Redux applications are highly
segmented, with various parts (actions, reducers, components, containers,
etc) all having their own home.  But they are pretty easy to grasp once
you understand what they do.  You'll also notice tht within each
folder, we've often broken things down by relevant subject area.  This
is purely for organizational ease.

### Components

Components are the most basic building block of React - they describe how
React should modify the DOM.  They are, at their heart, simply functions

```JavaScript
import React from 'react'

const OurComponent = ({ name }) => {
  return (
    <div>
      Hi, {name}.
    </div>
  )
}
```

or a little more concisely...

```JavaScript
import React from 'react'

const OurComponent = ({ name }) => (
  <div>
    Hi, {name}.
  </div>
)
```

We should always strive to keep our components, simple, and if possible pure
(i.e. have no internal state).  This makes them easy to test, easy to reason
about, and it's the direction the React team is going in optimizing for speed.

Sometimes that isn't possible though - and in fact most of the initially
committed React components (such as `Login`) are not pure - they have internal
state.  You can create a React component with state like so:

```JavaScript
import React, { Component } from 'react'

class TheirComponent extends Component {
  state = {
    count: 0
  }

  render() {
    return (
      <div onClick={() => this.setState({ count: this.state.count++ })}>
        I've been clicked {this.state.count} times.
      </div>
    )
  }
}
```

One note - any time you are using JSX (the syntax which allows us to mix HTML
and JS) you should `import React from 'react'`, even if you don't call it in
the scope.

Of course your stateful components can get quite complicated, and React offers
a variety of lifecycle methods to hook into to enable you to achieve various
feats. But internal component state is where bugs live, or at least the harder
to spot ones, so it is best avoided.  A good way to tell when to use internal
state vs Redux state is when the state is tied only to the component itself
and woudldn't be missed if it disappeared. For example, a check box, or text
input.  You'll also see it used sometimes to control which sub-components are
viewable (see `Login` for an example of this pattern).

The other thing we often avoid in components is access our action functions
directly.  We usually act on functions passed in via arguments to the
Component.  Where do these (as well as the aforementioned data) come from?
Glad you asked...

### Containers

So if we are striving to avoid having a lot of logic and state in our
components, how do we get them in there? The common pattern for this is a
container, which effectively connects the dumb component to our Redux store
via the `connect` function.  This function takes two arguments,
`mapStateToProps` and `mapDispatchToProps` which are functions themselves
which returns Objects which are combined to form the "props" to our component,
essentially the arguments.

In our example pure component above `OurComponent`, we take a prop `name`.
So you might `connect` it like so:

```JavaScript
const OurConnectedComponent = connect((state) => ({ name: state.name }))(OurComponent)
```

You can now use this component elsewhere in the application, as you would
have directly, but now it will have the data it needs.  Generally you'll
strike a balance between simply passing down needed adata a couple levels
and creating containers.

### Actions

Actions describe how our state will change.  An action is a JavaScript Object,
and ours have a few standard keys:

```JavaScript
{
  type: 'DO_SOMETHING',
  payload: { our: 'data' }
}
```

There are a couple of other potential standard keys - a key `error` will be set
if the `payload` is a JavaScript `Error` object, and as a result the message
will be stored in our store under a special location for displaying errors.
There are other example, as in how we handle automatically setting and
unsetting our loading state, but we don't need to go into such details.

The key thing to understand is that you will use actions by calling the
`dispatch` function - which is accessible in the `mapDispatchToProps`
function when connecting a component.  The action Object is generated
by calling an action function, and passing the result into `dispatch`.

For example, we call `login` like so:

```JavaScript
dispatch(login({ mobile: '+123456', pin: '1234' }))
```

Inside login, it nows how to take the passed data, and generate the action
Object.  It might also (it does!) call other actions.

### Reducers

Reducers take the generated actions and then modify the store.  The store
should never be modified directly in these reducers - you should make a copy
of it and then return the copy.  The most common pattern for a reducer is a
`switch`/`case` statement, where we do different things based on what the
action type is:

```JavaScript
export default (state = {}, action = {}) => {
  switch (action.type) {
    case 'SET_SESSION':
      return Object.assign({}, state, action.payload)
    case 'CLEAR_SESSION':
      return {}
    default:
      return state
  }
}
```

As you can see they are simply functions. Each reducer function is combined
together, and namespaced.  For example, in the reducer above, if we
received the `SET_SESSION` action with data `{ foo: 'bar' }`, the return
value would be whatever was already in `state.session`, plus our new data.
So we'd be able to get `state.session.foo` out of the store.  Which brings us
to...

### Selectors

Think of selectors as the API to our store.  They ensure that we're always
accessing our data in the same way, so that if we need to change the schema of
our store that it's quite easy to do so in one place.  We can also memoize
them if need be, or do more complicated modifications before returning the
result.  Essentially they are just functions that take the state and return
a value.

```JavaScript
export const loading = state => state.globals.loading
```

### Store

You've heard a lot about the store, and here we are.  Functionality here
largely revovles around setting up the store, and also ensuring that we can
persist it to `localStorage` and then "rehydrate" it when a user returns to
the page at a later date.  It's also useful for providing default values
to our application in the event the user has never been here or cleared
`localStorage`.  You can provide default values in the `configureStore`
function.

### API

The last place of interest is the API.  This handles standardizing how we make
calls to our external services.  It's largely similar to the mobile app, and
in fact some of the code was lifted directly from there.

### Config.js

Finally, we'll take a look at Config.js, which handles configuration.  Unlike
the mobile app, in which we have multiple configurations based on the `ENV`
value, this lifts all configuration directly from environment variables.

This means we can stamp values in CI for when we build the application, and
locally we can use a `.env` file to drive configuration.
