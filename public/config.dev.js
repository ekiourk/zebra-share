window.ZEBRA_CONFIG = {
    VERSION: '0.1.0',
    ENV: 'development',
    SERVER_URL: 'https://testapi.zebrapp.io',
    WS_SERVER_URL: 'wss://testapi.zebrapp.io',
    MIXPANEL_TOKEN: '2afb66eab477f1ba585634e1db32a9df',
    SENTRY_URL: 'https://10cb09577aae4b049a52287ed8b9ab13@sentry.io/1241525',
};
