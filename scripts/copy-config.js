const fs = require('fs-extra');
const path = require('path');

const VERSION = process.env.REACT_APP_VERSION;
if (!VERSION) {
    throw new Error('No REACT_APP_VERSION is defined');
}

fs.readdir('config', function(err, files) {
    if (err) {
        // eslint-disable-next-line no-console
        console.error('Could not list the config directory.', err);
        process.exit(1);
    }
    files.forEach(function(file) {
        const filename = file.split('.')[0];
        fs.copySync(path.join('config', file), `build/config/${filename}/config.${VERSION}.js`, {
            overwrite: true,
        });
    });
    // copy the testing file on the build folder so it will work for local builds
    fs.copySync(path.join('config', 'testing.js'), `build/config.${VERSION}.js`, {
        overwrite: true,
    });
});
