const cp = require('child_process');
const { version } = require('../package.json');

const hash = cp
    .execSync('git rev-parse --short HEAD')
    .toString()
    .trim();

// eslint-disable-next-line no-console
console.log(process.env.BUILD_VERSION || `${version}-${hash}`);
