import React from 'react';
import { ApolloProvider } from 'react-apollo';

import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';

import { split } from 'apollo-link';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';

const ApolloWrapper = ({
    tokenAuth,
    config: { SERVER_URL: serverUrl, WS_SERVER_URL: wsServerUrl },
    children,
}) => {
    // Create an http link:
    const httpLink = new HttpLink({
        uri: `${serverUrl}/graphql/`,
        headers: {
            Authorization: `Token ${tokenAuth}`,
        },
    });

    // Create a WebSocket link:
    const wsLink = new WebSocketLink({
        uri: `${wsServerUrl}/graphql-websocket/${tokenAuth}`,
        options: {
            reconnect: true,
            reconnectionAttempts: 10,
            lazy: true,
        },
    });

    wsLink.subscriptionClient.maxConnectTimeGenerator.duration = () =>
        wsLink.subscriptionClient.maxConnectTimeGenerator.max;

    // using the ability to split links, you can send data to each link
    // depending on what kind of operation is being sent
    const link = split(
        // split based on operation type
        ({ query }) => {
            const definition = getMainDefinition(query);
            return (
                definition.kind === 'OperationDefinition' && definition.operation === 'subscription'
            );
        },
        wsLink,
        httpLink,
    );

    const client = new ApolloClient({
        link,
        cache: new InMemoryCache(),
    });

    return <ApolloProvider client={client}>{children}</ApolloProvider>;
};

export default ApolloWrapper;
