import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import styled from 'styled-components/macro';

import storeSvc from './store';
import RegisterReferred from './pages/RegisterReferred';
import Congratulations from './pages/Congratulations';
import Track from './pages/Track';

const Container = styled.div`
    min-height: 100%;
    height: 100%;
    width: 100%;
`;

const ShareApp = ({ config, children }) => {
    const store = storeSvc.createStore(config);

    return (
        <Provider store={store}>
            <Router>
                <Container>
                    <Route exact path="/" component={RegisterReferred} />
                    <Route exact path="/ref" component={RegisterReferred} />
                    <Route exact path="/congratulations" component={Congratulations} />
                    <Route exact path="/track/:accessToken" component={Track} />
                    {children}
                </Container>
            </Router>
        </Provider>
    );
};

export default ShareApp;
