import { sdk as sentrySDK } from '@zebra/utils/dist/sentry';

const fetchHelper = async (url, options = {}, isFormData) => {
    const fetchOptions = {
        method: 'GET',
        ...options,
    };

    if (fetchOptions.body && !isFormData) {
        fetchOptions.body = window.JSON.stringify(fetchOptions.body);
    }

    const response = await fetch(window.ZEBRA_CONFIG.SERVER_URL + url, fetchOptions);

    if (response.status >= 400) {
        const message = await response.json();

        const error = {
            message: `Bad response from server at ${response.url} => ${response.status}, ${
                response.statusText
            }`,
            url: response.url,
            status: response.status,
            statusText: response.statusText,
        };

        error.message = message.error;

        sentrySDK.captureException(error.message, { extra: error });
        // eslint-disable-next-line prefer-promise-reject-errors
        return Promise.reject({ ...error, ...message });
    }

    if (response.ok && response.status === 204) {
        return Promise.resolve('Success');
    }

    return response.json();
};

export default fetchHelper;
