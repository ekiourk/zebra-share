import FormData from 'form-data';

const objectToForm = object => {
    const form = new FormData();
    Object.entries(object).forEach(([k, v]) => form.append(k, v));
    return form;
};

export default objectToForm;
