import * as urls from './urls';
import fetchHelper from './fetchHelper';
import objectToForm from './objectToForm';

const registerAPI = async data =>
    fetchHelper(
        urls.register,
        {
            method: 'POST',
            body: objectToForm(data),
        },
        true,
    );

export default registerAPI;
