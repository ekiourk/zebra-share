import * as urls from './urls';
import fetchHelper from './fetchHelper';

const getShareCode = async code => fetchHelper(urls.shareCodeInfo(code));

export default getShareCode;
