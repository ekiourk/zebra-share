import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import FlagIcon from './index';

test('<FlagIcon should render correctly />', () => {
    const renderer = shallow(<FlagIcon number="+447000123123" />);
    const tree = toJson(renderer);
    expect(tree).toMatchSnapshot();
});
