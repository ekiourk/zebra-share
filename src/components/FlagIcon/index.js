import React from 'react';
import { parse } from 'libphonenumber-js';
import ReactCountryFlag from 'react-country-flag';

const FlagIcon = ({ number }) =>
    !!parse(number).country && (
        <ReactCountryFlag svg code={parse(number).country} styleProps={{ width: 24 }} />
    );

export default FlagIcon;
