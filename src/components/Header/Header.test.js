import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import _noop from 'lodash/noop';
import Header from './index';

test('<Header should render correctly />', () => {
    const renderer = shallow(<Header actionIconClick={_noop} styleAction={null} />);
    const tree = toJson(renderer);
    expect(tree).toMatchSnapshot();
});
