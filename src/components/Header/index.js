import React from 'react';
import styled from 'styled-components/macro';
import { ReactComponent as DropIcon } from '../../images/drop.svg';

const iOSStatusBarHeight = (window.device || {}).platform === 'iOS' ? 20 : 0;
const headerHeightNumber = 93 + iOSStatusBarHeight;
const colorNav = '#000000';

const Container = styled.div`
    justify-content: center;
    background-color: ${colorNav};
    align-items: center;
    height: ${`${headerHeightNumber - iOSStatusBarHeight}px`};
    position: relative;
    margin-top: 0;
    margin-bottom: 30px;
    margin-left: 0;
    margin-right: 0;
    z-index: 20;
    display: flex;
    width: 100%;
    box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.15);
`;

const Header = () => (
    <Container id="header">
        <DropIcon width="26" height="45" />
    </Container>
);

export default Header;
