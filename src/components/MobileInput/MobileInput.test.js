import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import MobileInput from './index';

test('<MobileInput should render correctly />', () => {
    const renderer = shallow(<MobileInput value="+4407927755544" />);
    const tree = toJson(renderer);
    expect(tree).toMatchSnapshot();
});
