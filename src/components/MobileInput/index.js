import React, { Component } from 'react';
import { parse } from 'libphonenumber-js';
import styled from 'styled-components/macro';
import { Form } from 'semantic-ui-react';
import FlagIcon from '../FlagIcon';

const InputWrapper = styled.div`
    position: relative;
`;

const StyledIconWrapper = styled.div`
    position: absolute;
    top: 8px;
    left: 1em;
    z-index: 5;
`;

const StyledInput = styled(Form.Input)`
    &&&& {
        input {
            ${({ country }) => `${country && 'padding-left: 3em'}`};
        }
    }
`;

class MobileInput extends Component {
    state = { isFocused: false };

    onFocusInput = () => {
        this.setState({ isFocused: true });
    };

    onBlurInput = () => {
        this.setState({ isFocused: false });
    };

    render() {
        const { value, ...props } = this.props;

        const { country } = parse(value);

        const { isFocused } = this.state;

        return (
            <InputWrapper>
                {country && (
                    <StyledIconWrapper>
                        <FlagIcon number={value} />
                    </StyledIconWrapper>
                )}

                <StyledInput
                    {...props}
                    onFocus={e => {
                        this.onFocusInput(e);
                    }}
                    onBlur={e => {
                        this.onBlurInput(e);
                    }}
                    value={isFocused ? value || '+44' : value}
                    type="tel"
                    placeholder="Mobile Number"
                    country={country}
                    maxLength="16"
                    {...props}
                />
            </InputWrapper>
        );
    }
}

export default MobileInput;
