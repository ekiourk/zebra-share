import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Referrer from './index';

test('Referrer renders correctly', () => {
    const renderer = shallow(<Referrer first_name="John" last_name="Doe" referral_amount="12" />);
    const tree = toJson(renderer);
    expect(tree).toMatchSnapshot();
});
