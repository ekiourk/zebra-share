import React from 'react';
import { Segment } from 'semantic-ui-react';
import styled from 'styled-components/macro';

const ReferrerSegment = styled(Segment)`
    margin: 15px;
`;

export default function Referrer(props) {
    const { first_name, last_name, referral_amount } = props;

    return (
        <ReferrerSegment
            raised
            loading={!first_name && !last_name && !referral_amount}
            textAlign="center"
        >
            {`You've been invited by ${first_name} ${last_name} to join Zebra Fuel.`}
            <br />
            {`Sign up for an account now to get £${referral_amount} off your first purchase.`}
        </ReferrerSegment>
    );
}
