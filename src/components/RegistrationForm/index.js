/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/macro';
import _isEmpty from 'lodash/isEmpty';
import _mapValues from 'lodash/mapValues';
import _omit from 'lodash/omit';
import { Ref, Button, Message, Form, Checkbox } from 'semantic-ui-react';
import isEmail from 'validator/lib/isEmail';
import isMobilePhone from 'validator/lib/isMobilePhone';
import { colorScarlet, tinyText } from '../../constants/styles';
import MobileInput from '../MobileInput';

const Terms = styled.div`
    display: flex;

    .ui.checkbox {
        margin-right: 10px;
    }
`;

const ErrorText = styled.div`
    left: 0;
    right: 0;
    margin-top: 1em;
    text-align: right;
    color: ${colorScarlet};
    font-size: ${tinyText};
    top: 3.2rem;
`;

const StyledForm = styled(Form)`
    width: 190px;
    padding: 10px;
`;

export class RegistrationForm extends React.Component {
    state = {
        terms: false,
        mobile: '',
        errors: { first_name: '', last_name: '', email: '', mobile: '' },
    };

    inputs = {};

    handleChange = ({ target: { name, value } }) => {
        let error = null;

        if (!value) {
            error = 'Required';
        } else {
            switch (name) {
                case 'email':
                    if (!isEmail(value)) {
                        error = 'Invalid email address';
                    }
                    break;
                case 'mobile':
                    if (!isMobilePhone(value, 'any', { strictMode: true })) {
                        error = 'Invalid mobile number';
                    }
                    break;
                default:
                    if (value.length < 1) {
                        error = 'Must be 1 character or more';
                    }
            }
        }

        if (error == null) {
            this.setState(({ errors }) => ({ errors: _omit(errors, name) }));
        } else if (this.state.errors[name] !== error) {
            this.setState(({ errors }) => ({ errors: { ...errors, [name]: error } }));
        }
    };

    handleRef = node => {
        if (node) {
            const [input] = node.getElementsByTagName('input');
            this.inputs[input.name] = input;
        }
    };

    handleSubmit = () =>
        this.props.onSubmit({ ...this.state, ..._mapValues(this.inputs, 'value') });

    render() {
        const {
            props: { loading, error },
            state: { mobile, errors, terms },
        } = this;

        return (
            <StyledForm onSubmit={this.handleSubmit} loading={loading} error>
                <Message content={error} error />
                <Form.Field>
                    <label htmlFor="first_name">First Name</label>
                    <Ref innerRef={this.handleRef}>
                        <Form.Input
                            placeholder="First Name"
                            name="first_name"
                            autoComplete="first_name"
                            onChange={this.handleChange}
                            error={!!errors.first_name}
                        />
                    </Ref>
                    <ErrorText>{errors.first_name}</ErrorText>
                </Form.Field>
                <Form.Field>
                    <label htmlFor="last_name">Last Name</label>
                    <Ref innerRef={this.handleRef}>
                        <Form.Input
                            placeholder="Last Name"
                            name="last_name"
                            autoComplete="last_name"
                            onChange={this.handleChange}
                            error={!!errors.last_name}
                        />
                    </Ref>
                    <ErrorText>{errors.last_name}</ErrorText>
                </Form.Field>
                <Form.Field>
                    <label htmlFor="mobile">Mobile Number</label>
                    <MobileInput
                        value={mobile}
                        name="mobile"
                        autoComplete="mobile"
                        onChange={e => {
                            this.setState({ mobile: e.target.value });
                            this.handleChange(e);
                        }}
                    />
                    <ErrorText>{errors.mobile}</ErrorText>
                </Form.Field>
                <Form.Field>
                    <label htmlFor="email">Email Address</label>
                    <Ref innerRef={this.handleRef}>
                        <Form.Input
                            placeholder="Email Address"
                            name="email"
                            autoComplete="email"
                            onChange={this.handleChange}
                            error={!!errors.email}
                        />
                    </Ref>
                    <ErrorText>{errors.email}</ErrorText>
                </Form.Field>

                <Form.Field>
                    <Terms>
                        <Checkbox
                            name="terms"
                            checked={terms}
                            onChange={() => this.setState(state => ({ terms: !state.terms }))}
                        />
                        <div>
                            I have read and accepted the&nbsp;
                            <a
                                href="https://zebra-fuel.com/terms-conditions.html"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                Terms & Conditions
                            </a>
                            &nbsp;and&nbsp;
                            <a
                                href="https://zebra-fuel.com/privacy-policy.html"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                Privacy Policy
                            </a>
                        </div>
                    </Terms>
                </Form.Field>
                <Button type="submit" disabled={loading || !terms || !_isEmpty(errors)}>
                    Submit
                </Button>
            </StyledForm>
        );
    }
}

RegistrationForm.propTypes = { onSubmit: PropTypes.func.isRequired };

export default RegistrationForm;
