export const getScreenSize = () => {
    if (window.innerWidth > 375) return 'large';
    if (window.innerWidth > 330) return 'medium';
    return 'small';
};

const getFontSize = (large, medium, small) =>
    `${
        {
            small,
            medium,
            large,
        }[getScreenSize()]
    }px`;

export const tinyText = getFontSize(14, 12, 10);
