/* eslint-disable import/no-extraneous-dependencies */

import 'es7-object-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { Icon } from 'leaflet';
import { config as sentryConfig } from '@zebra/utils/dist/sentry';

import 'semantic-ui-less/semantic.less';
import 'leaflet/dist/leaflet.css';
import iconUrl from 'leaflet/dist/images/marker-icon.png';
import iconRetinaUrl from 'leaflet/dist/images/marker-icon-2x.png';
import shadowUrl from 'leaflet/dist/images/marker-shadow.png';

import mixpanel from './utils/mixpanel';
import ShareApp from './ShareApp';

// https://github.com/PaulLeCam/react-leaflet/issues/255#issuecomment-388492108
delete Icon.Default.prototype._getIconUrl;
Icon.Default.mergeOptions({ iconRetinaUrl, iconUrl, shadowUrl });

const { ZEBRA_CONFIG } = window;
const { REACT_APP_VERSION } = process.env;

mixpanel.config(ZEBRA_CONFIG);

if (ZEBRA_CONFIG.ENV !== 'development') {
    sentryConfig({ ...ZEBRA_CONFIG, REACT_APP_VERSION });
}

ReactDOM.render(<ShareApp config={ZEBRA_CONFIG} />, document.getElementById('root'));
