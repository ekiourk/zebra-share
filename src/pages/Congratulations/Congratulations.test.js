import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import _noop from 'lodash/noop';

import { CongratulationsPage } from './index';

test('Congratulations renders correctly', () => {
    const renderer = shallow(<CongratulationsPage login={_noop} clearError={_noop} />);
    const tree = toJson(renderer);
    expect(tree).toMatchSnapshot();
});
