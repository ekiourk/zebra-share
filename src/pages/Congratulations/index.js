import React from 'react';
import styled from 'styled-components/macro';
import AppStoreLogo from './appstore.svg';
import PlayStoreLogo from './playstore.png';
import Background from './background.png';
import Cup from './cup.png';

const Wrapper = styled.div`
    display: flex;
    height: 100vh;
    align-items: center;
    justify-content: center;
    background-image: ${`url(${Background})`};
    background-repeat: no-repeat;
    background-color: #303540;
    background-size: cover;
`;

const Modal = styled.div`
    width: 100%;
    max-width: 530px;
    margin: 20px;
    overflow: hidden;
    box-shadow: 0 60px 80px 0 rgba(0, 0, 0, 0.4);
`;

const ImageResponsive = styled.img`
    max-width: 100%;
    display: block;
    margin: 0 auto 45px;
`;

const ModalTop = styled.div`
    padding: 30px 30px 20px;
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    background: #fbfbfb;
    @media (min-width: 456px) {
        padding: 69px 0 45px;
    }
`;

const ModalHeader = styled.div`
    margin-bottom: 10px;
    font-size: 25.5px;
    letter-spacing: 2px;
    text-align: center;
`;

const ModalSubHeader = styled.div`
    max-width: 350px;
    margin: 0 auto;
    font-size: 19px;
    line-height: 1.3;
    letter-spacing: 1.25px;
    text-align: center;
    color: #999;
`;

const ModalFooter = styled.div`
    display: flex;
    justify-content: space-evenly;
    align-items: center;
    flex-wrap: wrap;
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
    background: #fff;
`;

const ModalButton = styled.a`
    margin: 10px;
    &:hover {
        opacity: 0.9;
    }
`;

export const CongratulationsPage = () => (
    <Wrapper>
        <Modal>
            <ModalTop>
                <ImageResponsive src={Cup} alt="Trophy" />
                <ModalHeader>Congratulations</ModalHeader>
                <ModalSubHeader>
                    You have successfully registered, and got the bonus in your account.
                </ModalSubHeader>
            </ModalTop>
            <ModalFooter>
                <ModalButton
                    target="_blank"
                    rel="noopener noreferrer"
                    key="appstore"
                    href="https://itunes.apple.com/us/app/zebra-fuel/id1131087332"
                >
                    <img alt="appstore" width="135" height="40" src={AppStoreLogo} />
                </ModalButton>
                <ModalButton
                    target="_blank"
                    rel="noopener noreferrer"
                    key="playstore"
                    href="https://play.google.com/store/apps/details?id=com.monoku.zebra.customer&hl=en_GB"
                >
                    <img alt="playstore" width="145" height="60" src={PlayStoreLogo} />
                </ModalButton>
            </ModalFooter>
        </Modal>
    </Wrapper>
);

export default CongratulationsPage;
