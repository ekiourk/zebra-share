import React from 'react';
import fetchMock from 'fetch-mock';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';

import '../../../public/config.local';
import { resolveCallbacksAndUpdate, simulateChange } from '../../test/utils';
import ShareApp from '../../ShareApp';
import Congratulations from '../Congratulations';
import RegisterReferred from './index';

const shareCode = 'AJqLvde535fgEGAnvsPK3k';

const userDetails = {
    first_name: 'James',
    last_name: 'Smith',
    email: 'james@test.com',
    mobile: '+447000000000',
};

const mountRegistrationPage = async () => {
    fetchMock.get('*', 204);
    const app = mount(<ShareApp config={window.ZEBRA_CONFIG} />);

    // navigate to RegisterReferred page
    app.find('Router[history]')
        .prop('history')
        .push(`/ref?share_code=${shareCode}`);
    await resolveCallbacksAndUpdate(app);
    return app;
};

const fillRegisterForm = (app, { mobile, ...inputs }) => {
    Object.entries(inputs).forEach(([name, value]) => simulateChange(app, name, value));
    const input = app.find('input[placeholder="Mobile Number"]');
    const target = input.instance();
    target.value = mobile;
    input.simulate('change', { target });
};

const getTexts = field =>
    field
        .children()
        .map(v => v.text())
        .filter(v => v)
        .join(' ');

describe('Register page', () => {
    beforeEach(() =>
        fetchMock.post('*', 204).get('begin:/config.', { SERVER_URL: 'http://localhost' }),
    );

    afterEach(() => fetchMock.restore());

    it('should display server errors', async () => {
        fetchMock.get(`end:/api/share-code/${shareCode}/`, {
            status: 404,
            body: { detail: 'Not found.' },
        });
        const app = await mountRegistrationPage();
        expect(app.find('.message').text()).toStrictEqual('Not found.');
    });

    it('should display client validation errors', async () => {
        const app = await mountRegistrationPage();
        fillRegisterForm(app, { first_name: '', last_name: '', email: '', mobile: '7' });
        expect(app.find('.field').map(getTexts)).toStrictEqual(
            expect.arrayContaining([
                'First Name Required',
                'Last Name Required',
                'Email Address Required',
                'Mobile Number Invalid mobile number',
            ]),
        );
        expect(app.find("Button[type='submit']")).toBeDisabled();
    });

    it('should disable submit button when terms and conditions checkbox is not checked', async () => {
        const app = await mountRegistrationPage();
        fillRegisterForm(app, userDetails);
        expect(app.find("Button[type='submit']")).toBeDisabled();
    });

    it('should disable submit button when email is wrong', async () => {
        const app = await mountRegistrationPage();
        fillRegisterForm(app, { ...userDetails, email: 'wrong@email' });
        simulateChange(app, 'terms');
        expect(app).toIncludeText('Invalid email');
        expect(app.find("Button[type='submit']")).toBeDisabled();
    });

    it('should register user successfully', async () => {
        fetchMock.get(`end:/api/share-code/${shareCode}/`, { referral_amount: 10.0 });
        const app = await mountRegistrationPage();

        // should render RegisterReferred page
        expect(window.location.pathname).toStrictEqual('/ref');
        expect(toJson(app.render())).toMatchSnapshot();
        expect(app.find(RegisterReferred)).toExist();
        expect(app.find(Congratulations)).not.toExist();

        // fill the form and accept
        fillRegisterForm(app, userDetails);
        simulateChange(app, 'terms');

        // should have an ENABLED submit button
        expect(app.find("Button[type='submit']")).not.toBeDisabled();

        // simulate form SUBMIT
        app.find('form').simulate('submit');
        await resolveCallbacksAndUpdate(app);

        // should POST the form data to the backend
        expect(fetchMock.lastUrl()).toStrictEqual(expect.stringContaining('/api/login/register/'));
        expect(fetchMock.lastOptions()).toStrictEqual(
            expect.objectContaining({
                method: 'POST',
                body: expect.objectContaining({
                    _streams: expect.arrayContaining([
                        true, // terms
                        '+447000000000',
                        'James',
                        'Smith',
                        'james@test.com',
                        shareCode,
                    ]),
                }),
            }),
        );

        // should go to Congratulations page after submit
        expect(window.location.pathname).toStrictEqual('/congratulations');
        expect(app.find(RegisterReferred)).not.toExist();
        expect(app.find(Congratulations)).toExist();
    });
});
