import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import _get from 'lodash/get';
import styled from 'styled-components/macro';
import QueryString from 'query-string';
import * as globals from '../../selectors/globals';
import clearError from '../../reducers/globals/action';
import register from '../../reducers/register/action';
import getShareCodeInfo from '../../reducers/shareCode/action';
import RegistrationForm from '../../components/RegistrationForm';
import Referrer from '../../components/Referrer';
import Header from '../../components/Header';

const Div = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 100%;
    width: 100%;
`;

class RegisterReferred extends React.Component {
    componentWillMount() {
        const { location } = this.props;

        this.props.clearError();
        this.query = QueryString.parse(location.search);
        this.props.getShareCodeInfo(this.query.share_code);
    }

    handleSubmit = async ({ errors: __, ...formData }) => {
        const {
            query = {},
            props: { error, history, ...props },
        } = this;
        if (error) props.clearError();
        try {
            await props.register({ ...formData, share_code: query.share_code });
            history.push('/congratulations');
        } catch (registrationError) {
            // eslint-disable-next-line no-console
            console.error(registrationError);
        }
    };

    render() {
        const { referrer, referral_amount } = this.props;
        return (
            <div>
                <a target="_blank" rel="noopener noreferrer" href="http://zebra-fuel.com">
                    <Header />
                </a>
                <Div>
                    <Referrer {...referrer} referral_amount={referral_amount} />
                    <RegistrationForm {...this.props} onSubmit={this.handleSubmit} />
                </Div>
            </div>
        );
    }
}

export default connect(
    state => ({
        error: globals.error(state),
        referrer: _get(state, 'shareCode.referrer'),
        referral_amount: _get(state, 'shareCode.referral_amount'),
    }),
    dispatch => bindActionCreators({ clearError, getShareCodeInfo, register }, dispatch),
)(withRouter(RegisterReferred));
