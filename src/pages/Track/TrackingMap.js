import React from 'react';
import styled from 'styled-components/macro';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import L, { DivIcon } from 'leaflet';
import ReactDOMServer from 'react-dom/server';
import moment from 'moment';
import _ from 'lodash';
import memoize from 'memoize-one';

import { ReactComponent as CallButtonIcon } from '../../images/Call_Button.svg';
import { ReactComponent as SMSButtonIcon } from '../../images/SMS_Button.svg';
import { ReactComponent as CustomerPinIcon } from '../../images/Customer_Pin.svg';
import { ReactComponent as ClockIcon } from '../../images/clock.svg';
import { ReactComponent as CheckIcon } from '../../images/Check.svg';

const MapContainer = styled.div`
    height: 100%;
    position: relative;
    .leaflet-container {
        width: 100%;
        height: 100%;
    }
`;

const TopBar = styled.div`
    position: absolute;
    top: 0;
    left: 0%;
    height: 100px;
    width: 100%;
    z-index: 999;
    background-image: linear-gradient(
        to top,
        rgba(255, 255, 255, 0.001) 0%,
        rgba(255, 255, 255, 1) 50%,
        rgba(255, 255, 255, 1) 100%
    );
    text-align: center;
    padding: 10px;

    h2 {
        font-size: 18px;
        font-weight: 600;
        margin-bottom: 0;
    }
`;

const OrderId = styled.div`
    color: #b2bec6;
    font-size: 12px;
`;

const OrderInfo = styled.div`
    width: calc(100% - 40px);
    height: 180px;
    background: #fff;
    position: absolute;
    bottom: 20px;
    left: 20px;
    z-index: 999;
    border-radius: 8px;
    font-size: 12px;
    color: #2c2c2c;
`;

const DriverName = styled.div`
    font-size: 15px;
`;

const ZebraPlate = styled.div`
    color: #b2bec6;
`;

const DeliveryAddressLabel = styled.div``;

const DeliveryAddress = styled.div`
    color: #b2bec6;
`;

const ZebraInfo = styled.div`
    position: absolute;
    left: 20px;
    top: 20px;
`;
const DeliveryAddressInfo = styled.div`
    position: absolute;
    left: 20px;
    bottom: 20px;
    width: 150px;
`;
const ContactControls = styled.div`
    position: absolute;
    right: 20px;
    top: 20px;
`;
const EstimatedArrivalInfo = styled.div`
    position: absolute;
    right: 20px;
    bottom: 20px;
`;
const EstimatedArrivalTime = styled.div`
    font-size: 24px;
    margin-bottom: 5px;
    font-weight: bold;
`;
const EstimatedArrivalLabel = styled.div`
    color: #b2bec6;
    font-size: 9px;
`;

const ContactButton = styled.a`
    border: 0;
    padding: 0;
    margin-left: 10px;
    background: none;
    outline: none;
`;

const ProviderMarker = styled.div`
    border-radius: 100%;
    width: 18px;
    height: 18px;
    background-image: ${({ selected }) =>
        selected
            ? 'linear-gradient(135deg, #656565, #111);'
            : 'linear-gradient(135deg, #ccc, #555);'};
`;

const CustomerMarker = styled(CustomerPinIcon)`
    width: 18px;
    height: 24px;
`;

const Clock = styled(ClockIcon)`
    margin-right: 5px;
`;

const Check = styled(CheckIcon)`
    margin-right: 5px;
`;

const EtaTime = styled.span`
    font-size: 11px;

    line-height: 2em;
`;

const EtaPopup = styled(Popup)`
    .leaflet-popup-content {
        margin: 5px;
        display: flex;
    }
`;

const filterByStates = memoize((items, states) => {
    return items.filter(({ node }) => {
        return states.includes(node.state);
    });
});

class TrackingMap extends React.PureComponent {
    mapRef = React.createRef();

    orderMarkerRef = React.createRef();

    providerMarkerRefs = {};

    state = { selectedTaskId: null };

    visibleStates = ['ARRIVED', 'ACCEPTED'];

    componentDidMount() {
        this.props.subscribeToOrderUpdate();

        const { latitude: orderLatitude, longitude: orderLongitude } = this.orderCoords;

        const providerBounds = this.trackingTasks.map(({ node: task }) => {
            const { latitude, longitude } = task.trackingProvider.location;
            return [latitude, longitude];
        });

        const bounds = this.getLatLngBounds([orderLatitude, orderLongitude], ...providerBounds);

        const { leafletElement } = this.mapRef.current;
        leafletElement.fitBounds(bounds, { padding: [30, 210] });
    }

    get trackingTasks() {
        const {
            data: {
                trackingOrder: { trackingTasks },
            },
        } = this.props;
        return filterByStates(trackingTasks.edges, this.visibleStates);
    }

    get selectedTask() {
        const { selectedTaskId } = this.state;
        const tasks = this.props.data.trackingOrder.trackingTasks.edges.map(({ node }) => node);
        return tasks.find(task => task.id === selectedTaskId);
    }

    get orderCoords() {
        const { data } = this.props;
        const { latitude: orderLatitude, longitude: orderLongitude } = data.trackingOrder.location;

        return { latitude: orderLatitude, longitude: orderLongitude };
    }

    getLatLngBounds = (...points) => {
        const latLngs = points.map(point => {
            return L.latLng(point[0], point[1]);
        });
        const bounds = L.latLngBounds(latLngs);
        return bounds;
    };

    handleReady = () => {
        this.trackingTasks.forEach(({ node: task }) => {
            this.providerMarkerRefs[task.id].leafletElement.openPopup();
        });

        this.setState({ selectedTaskId: this.trackingTasks[0].node.id });

        // remove default click event
        _.forEach(this.providerMarkerRefs, ref => {
            const { leafletElement } = ref;

            const {
                _events: { click },
            } = leafletElement;

            if (click[0].fn.name === '_openPopup') {
                leafletElement.removeEventListener('click', click[0].fn);
            }
        });
    };

    renderMarker = task => {
        const { selectedTaskId } = this.state;

        const { latitude, longitude } = task.trackingProvider.location;

        const { eta, state } = task;

        const etaDiff =
            eta !== null ? Math.round(moment.duration(moment(eta).diff(moment())).asMinutes()) : 0;

        const hasArrived = state === 'ARRIVED';

        return (
            <Marker
                key={task.id}
                ref={node => {
                    this.providerMarkerRefs[task.id] = node;
                }}
                position={[latitude, longitude]}
                onClick={() => {
                    this.setState({ selectedTaskId: task.id });
                }}
                icon={
                    new DivIcon({
                        iconSize: [14, 14],
                        iconAnchor: [7, 7],
                        popupAnchor: [0, -9],
                        className: 'provider-marker-div-icon',
                        html: ReactDOMServer.renderToString(
                            <ProviderMarker selected={selectedTaskId === task.id} />,
                        ),
                    })
                }
            >
                <EtaPopup
                    autoClose={false}
                    closeButton={false}
                    closeOnClick={false}
                    closeOnEscapeKey={false}
                >
                    {hasArrived ? <Check /> : <Clock />}

                    {hasArrived ? <span>Arrived</span> : <EtaTime>{`${etaDiff} mins`}</EtaTime>}
                </EtaPopup>
            </Marker>
        );
    };

    renderSelectedTask = () => {
        const {
            data: {
                trackingOrder: { location },
            },
        } = this.props;

        const { selectedTaskId } = this.state;

        if (!selectedTaskId) return null;

        const { selectedTask } = this;

        const { eta, state } = selectedTask;

        const hasArrived = state === 'ARRIVED';

        return (
            selectedTask && (
                <OrderInfo>
                    <ZebraInfo>
                        <DriverName>{selectedTask.trackingProvider.driverName}</DriverName>
                        <ZebraPlate>{selectedTask.trackingProvider.zebraPlate}</ZebraPlate>
                    </ZebraInfo>
                    <DeliveryAddressInfo>
                        <DeliveryAddressLabel>Delivery address</DeliveryAddressLabel>
                        <DeliveryAddress>{location.address}</DeliveryAddress>
                    </DeliveryAddressInfo>
                    <ContactControls>
                        <ContactButton href={`sms: ${selectedTask.trackingProvider.driverMobile}`}>
                            <SMSButtonIcon />
                        </ContactButton>
                        <ContactButton href={`tel: ${selectedTask.trackingProvider.driverMobile}`}>
                            <CallButtonIcon />
                        </ContactButton>
                    </ContactControls>
                    <EstimatedArrivalInfo>
                        <EstimatedArrivalTime>
                            {hasArrived ? 'Arrived' : moment(eta).format('HH:mm')}
                        </EstimatedArrivalTime>
                        <EstimatedArrivalLabel>
                            {hasArrived ? 'Driver is on location' : 'ESTIMATED ARRIVAL'}
                        </EstimatedArrivalLabel>
                    </EstimatedArrivalInfo>
                </OrderInfo>
            )
        );
    };

    render() {
        const { data } = this.props;

        const { latitude: orderLatitude, longitude: orderLongitude } = this.orderCoords;

        const {
            trackingOrder: { orderId },
        } = data;

        return (
            <MapContainer>
                <TopBar>
                    <h2>Track your zebra</h2>
                    <OrderId>{`Order #${orderId}`}</OrderId>
                </TopBar>

                {this.renderSelectedTask()}

                <Map
                    ref={this.mapRef}
                    height={600}
                    preferCanvas
                    maxZoom={19}
                    zoomControl={false}
                    whenReady={this.handleReady}
                >
                    <TileLayer
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>'
                        url="https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png"
                    />
                    <Marker
                        ref={this.orderMarkerRef}
                        position={[orderLatitude, orderLongitude]}
                        icon={
                            new DivIcon({
                                iconSize: [18, 24],
                                iconAnchor: [9, 18],
                                popupAnchor: [0, -20],
                                className: 'order-marker-div-icon',
                                html: ReactDOMServer.renderToString(<CustomerMarker />),
                            })
                        }
                    />

                    {this.trackingTasks.map(({ node }) => this.renderMarker(node))}
                </Map>
            </MapContainer>
        );
    }
}

export default TrackingMap;
