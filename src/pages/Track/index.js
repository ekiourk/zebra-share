import React from 'react';
import styled from 'styled-components/macro';
import gql from 'graphql-tag.macro';
import { Query } from 'react-apollo';

import ApolloContainer from '../../ApolloContainer';
import TrackingMap from './TrackingMap';
import { ReactComponent as LoadingIcon } from '../../images/Progress_Circle.svg';

const Loading = styled(LoadingIcon)`
    width: 30px;
    height: 30px;

    animation: spin 2s linear infinite;

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
`;

const Overlay = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    z-index: 999;
    width: 100%;
    height: 100%;
    background: rgba(255, 255, 255, 0.5);
    display: flex;
    align-items: center;
    justify-content: center;
`;

const ORDER_QUERY = gql`
    query OrderQuery {
        trackingOrder {
            id
            orderId
            state
            location {
                address
                latitude
                longitude
            }
            trackingTasks {
                edges {
                    node {
                        id
                        eta
                        state
                        trackingProvider {
                            id
                            location {
                                latitude
                                longitude
                            }
                            zebraPlate
                            driverMobile
                            driverName
                        }
                    }
                }
            }
        }
    }
`;

const TRACKING_ORDER_SUBSCRIPTION = gql`
    subscription TrackingOrderSubscription {
        trackingOrderSubscription {
            trackingOrder {
                id
                orderId
                state
                location {
                    address
                    latitude
                    longitude
                }
                trackingTasks {
                    edges {
                        node {
                            id
                            eta
                            state
                            trackingProvider {
                                id
                                location {
                                    latitude
                                    longitude
                                }
                                zebraPlate
                                driverMobile
                                driverName
                            }
                        }
                    }
                }
            }
        }
    }
`;

function Track(props) {
    return (
        <ApolloContainer tokenAuth={props.match.params.accessToken} config={window.ZEBRA_CONFIG}>
            <Query query={ORDER_QUERY}>
                {({ subscribeToMore, data, loading, error, ...result }) => {
                    if (loading)
                        return (
                            <Overlay>
                                <Loading />
                            </Overlay>
                        );
                    if (error) {
                        if (
                            (error.networkError && error.networkError.statusCode === 401) ||
                            error.message === 'GraphQL error: Unauthorized'
                        ) {
                            return <Overlay>Tracking link is invalid or it has expired.</Overlay>;
                        }
                        return <Overlay>Server error</Overlay>;
                    }

                    if (data.trackingOrder === null) {
                        return <Overlay>Tracking link has expired.</Overlay>;
                    }

                    return (
                        <TrackingMap
                            {...result}
                            data={data}
                            subscribeToOrderUpdate={() =>
                                subscribeToMore({
                                    document: TRACKING_ORDER_SUBSCRIPTION,
                                    updateQuery: (
                                        previousResult,
                                        {
                                            subscriptionData: {
                                                data: {
                                                    trackingOrderSubscription: { trackingOrder },
                                                },
                                            },
                                        },
                                    ) => {
                                        return {
                                            ...previousResult,
                                            trackingOrder,
                                        };
                                    },
                                })
                            }
                        />
                    );
                }}
            </Query>
        </ApolloContainer>
    );
}

export default Track;
