import { createAction } from 'redux-actions';
import _camelCase from 'lodash/camelCase';

const createAsyncActions = action => {
    const request = `${action}_REQUEST`;
    const success = `${action}_SUCCESS`;
    const failure = `${action}_FAILURE`;

    return {
        [_camelCase(request)]: createAction(request, undefined, () => ({ loading: true })),
        [_camelCase(success)]: createAction(success, undefined, () => ({ loading: false })),
        [_camelCase(failure)]: createAction(failure, undefined, () => ({ loading: false })),
    };
};

export default createAsyncActions;
