import { createAction } from 'redux-actions';

const clearError = createAction('CLEAR_ERROR');

export default clearError;
