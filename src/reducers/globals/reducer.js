export default (state = {}, action = {}) => {
    const newState = { ...state };

    // This ensures that any action with "loading" in its
    // meta gets reflected globally.
    if ('meta' in action && 'loading' in action.meta) {
        newState.loading = action.meta.loading;
    }

    // This ensures that any action which is an error has
    // its error message reflected.
    if (action.error) {
        newState.error = action.payload.message;
    }

    // The following are more traditional reducers
    switch (action.type) {
        case 'CLEAR_ERROR':
            newState.error = undefined;

            return newState;
        default:
            return newState;
    }
};
