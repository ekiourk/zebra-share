import { combineReducers } from 'redux';
import globals from './globals/reducer';
import shareCode from './shareCode/reducer';

export default combineReducers({
    globals,
    shareCode,
});
