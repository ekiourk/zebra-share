import createAsyncActions from '../createAsyncActions';
import registerAPI from '../../api/register';

const { registerRequest, registerSuccess, registerFailure } = createAsyncActions('REGISTER');

export const register = data => async dispatch => {
    dispatch(registerRequest());
    try {
        await registerAPI(data);
    } catch (e) {
        dispatch(registerFailure(new Error(e.message || e.detail)));
        throw e;
    }

    dispatch(registerSuccess());
};

export default register;
