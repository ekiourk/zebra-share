import createAsyncActions from '../createAsyncActions';
import getShareCode from '../../api/share';

const { shareCodeRequest, shareCodeSuccess, shareCodeFailure } = createAsyncActions('SHARE_CODE');

const getShareCodeInfo = (code, success, failure) => async dispatch => {
    if (code === undefined) {
        dispatch(shareCodeFailure(new Error('Share code not found.')));
    }

    dispatch(shareCodeRequest());

    try {
        const data = await getShareCode(code);
        dispatch(shareCodeSuccess(data));
        if (success) {
            success();
        }
    } catch (error) {
        const message = error.message || error.detail;
        dispatch(shareCodeFailure(new Error(message)));
        if (failure) {
            failure(error);
        }
    }
};

export default getShareCodeInfo;
