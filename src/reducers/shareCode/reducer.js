export default (state = {}, action = {}) => {
    switch (action.type) {
        case 'SHARE_CODE_REQUEST':
            return { state, loading: true };

        case 'SHARE_CODE_SUCCESS':
            return {
                ...state,
                loading: false,
                referrer: action.payload.referrer,
                referral_amount: action.payload.referral_amount,
            };
        case 'SHARE_CODE_FAILURE':
            return {
                ...state,
                loading: false,
                referrer: null,
                referral_amount: null,
                error: 'Error',
            };

        default:
            return state;
    }
};
