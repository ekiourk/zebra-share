export const loading = state => state.globals.loading;
export const version = state => state.globals.version;
export const error = state => state.globals.error;
