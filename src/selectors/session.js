export const isLoggedIn = state => state.auth && state.auth.tokenAuth;
export const username = state => state.session.userName;
export const unknownCars = state => state.session.unknownCars;
