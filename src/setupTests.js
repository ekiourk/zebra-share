import 'jest-enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { configure } from 'enzyme';

configure({ adapter: new Adapter() });

export default class LocalStorageMock {
    store = {};

    clear = () => {
        this.store = {};
    };

    getItem = key => this.store[key];

    setItem = (key, value) => {
        this.store[key] = String(value);
    };

    removeItem = key => {
        delete this.store[key];
    };
}

global.localStorage = new LocalStorageMock();
global.requestAnimationFrame = callback => setTimeout(callback, 0);

jest.mock('mixpanel-browser', () => ({
    identify: jest.fn(),
    track: jest.fn(),
}));
