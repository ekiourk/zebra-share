import configureStore from './configureStore';

export default {
    createStore: config =>
        configureStore({
            globals: {
                version: config.VERSION || 'development',
                loading: false,
            },
        }),
};
