import { combineReducers } from 'redux';
import globals from '../reducers/globals/reducer';
import shareCode from '../reducers/shareCode/reducer';

export default combineReducers({
    globals,
    shareCode,
});
