import React from 'react';
import { Portal } from 'semantic-ui-react';
import { mount, ReactWrapper } from 'enzyme';
import fetchMock from 'fetch-mock';
import ShareApp from '../ShareApp';

export const fillVariablesInQuery = (query, variables) => {
    const fullQuery = query;
    Object.keys(variables).forEach(variableName => {
        fullQuery.replace(`${variableName}`, variables[variableName]);
    });
};

export const getPortal = wrapper =>
    new ReactWrapper(
        wrapper
            .find(Portal)
            .filterWhere(v => v.instance().portalNode != null) // filter opened portals
            .first()
            .props().children,
    );

export const getTexts = wrapper => {
    // extract an strings array from tables, lists and spans
    const selector = ['td', '[role="listitem"]', 'span'].find(v => wrapper.find(v).length > 1);
    if (selector == null) return [wrapper.text().trim()];
    return wrapper
        .find(selector)
        .map(v => getTexts(v).join(' '))
        .filter(v => v);
};

export const resolveCallbacksAndUpdate = async wrapper => {
    await new Promise(r => setTimeout(r, 0));
    await fetchMock.flush();
    await new Promise(r => setTimeout(r, 0));
    if (wrapper) wrapper.update();
};

export const simulateChange = (wrapper, name, value) => {
    const input = wrapper.find(`input[name="${name}"]`);
    const target = input.instance();
    target.value = value;
    input.simulate('change', { target });
};

export const getLoggedInApp = async () => {
    const app = mount(<ShareApp config={window.ZEBRA_CONFIG} />);
    await resolveCallbacksAndUpdate(app);
    return app;
};

export const goPage = async (app, pageName) => {
    if (!app.find(`h1[children="${pageName}"]`).exists()) {
        app.find('a[href]')
            .filterWhere(v => v.text() === pageName)
            .first()
            .simulate('click', { button: 0 });
        await resolveCallbacksAndUpdate(app);
    }
};

export const goToUrl = async (app, url) => {
    app.find('Router[history]')
        .prop('history')
        .push(url);
    await resolveCallbacksAndUpdate(app);
};
