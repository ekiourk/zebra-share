/* eslint-disable no-console */
import mixpanel from 'mixpanel-browser';

const config = configuration => {
    if (configuration.MIXPANEL_TOKEN) {
        mixpanel.init(configuration.MIXPANEL_TOKEN, { persistence: 'localStorage' });
        mixpanel.register({
            version: configuration.VERSION,
            environment: configuration.ENV,
        });
        window.mixpanel = mixpanel;
    } else {
        console.warn('No MIXPANEL_TOKEN set - skipping Mixpanel setup.');
        window.mixpanel = {
            identify: data => console.info('Mixpanel identify called: ', data),
            track: message => console.info('Mixpanel: ', message),
        };
    }
};

export default { config };
