#!/bin/bash

aws s3 sync build s3://register-app-builds.zebrapp.io/$REACT_APP_VERSION/ --region=$AWS_DEFAULT_REGION --acl public-read
aws s3 sync build s3://register-app-builds.zebrapp.io/latest_master/ --region=$AWS_DEFAULT_REGION --acl public-read --delete
